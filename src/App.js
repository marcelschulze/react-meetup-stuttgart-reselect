import React, { Component } from 'react';
import { connect } from 'react-redux';

import './App.css'

import ListView from './containers/ListView';
import MapView from './containers/MapView';

class App extends Component {
  render() {
    console.info('render App');
    return (
      <div className="App">
        <div className="App-header">
          <h3 onClick={this.props.changeSomething} >Welcome to Reselect {this.props.counter}</h3>
        </div>
        <ListView category='A' />
        <ListView category='B' />
        <MapView />
      </div>
    );
  }
}

const mapState = (state) => ({
  counter: state.counter,
});

const mapDispatch = (dispatch) => ({
  changeSomething: () => dispatch({ type: 'CHANGE_SOMETHING' }),
});

export default connect(mapState, mapDispatch)(App);
