import React from 'react';
import ReactDOM from 'react-dom';
import ReduxContainer from './ReduxContainer';
import './index.css';

ReactDOM.render(
  <ReduxContainer />,
  document.getElementById('root')
);
