import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getMapMarkers } from '../logic/reducer';

class MapView extends Component {
  render() {
    console.info('render MapView');
    const { mapMarkers } = this.props;

    return (
      <div className='map-view'>
        <h3>MapView</h3>
        { mapMarkers.map((m) => (
            <div key={m.lat + m.lng}>{m.lat} & {m.lng}</div>
        ))}
      </div>
    );
  }
}

const mapState = (state) => ({
    mapMarkers: getMapMarkers(state),
});

const mapDispatch = (dispatch) => ({

});

export default connect(mapState, mapDispatch)(MapView);
