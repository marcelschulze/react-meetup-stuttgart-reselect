import React, { Component } from 'react';
import { connect } from 'react-redux';

import { makeGetRestaurantCollectionByCategory } from '../logic/reducer';

class ListView extends Component {
  render() {
    console.info('render ListView');
    const { restaurants } = this.props;

    return (
      <div className='list-view'>
        <h3>ListView</h3>
        { restaurants.map((r) => (
          <div key={r.id}>{r.name}</div>
        ))}
      </div>
    );
  }
}

const makeMapStateToProps = () => {
  const getRestaurantCollection = makeGetRestaurantCollectionByCategory();

  return (state, ownProps) => ({
    restaurants: getRestaurantCollection(state, ownProps),
  });
}

const mapDispatch = (dispatch) => ({

});

export default connect(makeMapStateToProps, mapDispatch)(ListView);
