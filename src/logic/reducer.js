import {combineReducers} from 'redux';
import {createSelector} from 'reselect';

const initialRestaurants = {
  1: { id: 1, name: 'Name 1', geo: { lat: 13, lng: 14 }, category: 'A' },
  2: { id: 2, name: 'Name 2', geo: { lat: 23, lng: 24 }, category: 'A' },
  3: { id: 3, name: 'Name 3', geo: { lat: 33, lng: 34 }, category: 'B' },
  4: { id: 4, name: 'Name 4', geo: { lat: 43, lng: 44 }, category: 'B' },
}

const restaurants = (state = initialRestaurants, action) => {
  switch (action.type) {
    default:
      return state;
  }
}

const counter = (state = 0, action) => {
  switch (action.type) {
    case 'CHANGE_SOMETHING': {
      return state + 1;
    }

    default:
      return state;
  }
}

export default combineReducers({
  entities: combineReducers({
    restaurants,
  }),
  counter,
});

export const toArray = (obj) => Object.keys(obj).map((key) => obj[key]);

/* ===============================================================================
basic selectors
=============================================================================== */

export const getRestaurants = (state) => state.entities.restaurants;

// export const getRestaurantCollection = (state) => toArray(getRestaurants(state))

export const getRestaurantCollection = createSelector(
  getRestaurants,
  (restaurants) => toArray(restaurants)
)

export const makeGetRestaurantCollectionByCategory = () => createSelector(
  getRestaurantCollection,
  (_, props) => props.category,
  (restaurants, category) => restaurants.filter((r) => r.category === category)
)

export const getMapMarkers = createSelector(
  getRestaurantCollection,
  (restaurants) => restaurants.map((r) => ({
      lat: r.geo.lat,
      lng: r.geo.lng,
    }))
);



