import {createStore, applyMiddleware} from 'redux';
import { createLogger } from 'redux-logger';

import reducer from './reducer';

const middlewares = [];

if(process.env.NODE_ENV === 'development') {
  const logger = createLogger({
    collapsed: (getState, action) => true,
    duration: true,
    diff: true,
  });
  middlewares.push(logger);
}

export default function configureStore(initialState) {
  const store = createStore(
    reducer,
    initialState,
    applyMiddleware(...middlewares)
  );

  return store;
}
