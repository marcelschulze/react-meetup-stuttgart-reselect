import React, {Component} from 'react';
import {Provider} from 'react-redux';

import App from './App';
import configureStore from './logic/configureStore';

const store = configureStore();

// bootstrap redux
export default class ReduxContainer extends Component {

  render() {
    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }
}

